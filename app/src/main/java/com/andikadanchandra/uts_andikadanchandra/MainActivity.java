package com.andikadanchandra.uts_andikadanchandra;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.ll_biodata) LinearLayout llBiodata;
    @BindView(R.id.ll_konversi_suhu) LinearLayout llKonversi;
    @BindView(R.id.ll_exit) LinearLayout llExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("UTS Andika dan Chandra");
        llBiodata.setOnClickListener(this);
        llKonversi.setOnClickListener(this);
        llExit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_biodata :
                startActivity(new Intent(this, BiodataActivity.class));
                break;
            case R.id.ll_konversi_suhu :
                startActivity(new Intent(this, KonversiActivity.class));
                break;
            case R.id.ll_exit :
                this.finish();
                break;
        }
    }
}
