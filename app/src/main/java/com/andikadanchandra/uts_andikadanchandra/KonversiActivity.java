package com.andikadanchandra.uts_andikadanchandra;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KonversiActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    @BindView(R.id.tv_celcius) TextView tvCelcius;
    @BindView(R.id.tv_fahrenheith) TextView tvFahrenheith;
    @BindView(R.id.img_switch) ImageView imgSwitch;
    @BindView(R.id.et_input) EditText etInput;
    @BindView(R.id.tv_hasil) TextView tvHasil;
    private boolean isCelcius = true;
    private Animation anim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konversi);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Temperature Converter");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        anim = AnimationUtils.loadAnimation(this, R.anim.anim_fade);

        imgSwitch.setOnClickListener(this);
        etInput.addTextChangedListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.img_switch){
            if(isCelcius){
                tvCelcius.setText(R.string.fahrenheit);
                tvCelcius.startAnimation(anim);
                tvFahrenheith.setText(R.string.celcius);
                tvFahrenheith.startAnimation(anim);
                isCelcius = false;
                hasil();
            } else {
                tvCelcius.setText(R.string.celcius);
                tvCelcius.startAnimation(anim);
                tvFahrenheith.setText(R.string.fahrenheit);
                tvFahrenheith.startAnimation(anim);
                isCelcius = true;
                hasil();
            }
        }
    }

    private double celciusToFahrenheith(double Input){
        return 32 + Input * 9/5;
    }

    private double fahrenheithToCelcius(double Input){
        return (Input - 32) * 5/9;
    }

    private void hasil(){
        switch (etInput.getText().toString()) {
            case "":
                tvHasil.setText("");
                break;
            case "-":
                tvHasil.setText("");
                break;
            default:
                double input = Double.parseDouble(etInput.getText().toString());
                double hasil;
                if(isCelcius){
                    hasil = celciusToFahrenheith(input);
                } else {
                    hasil = fahrenheithToCelcius(input);
                }
                tvHasil.setText(String.valueOf(new DecimalFormat("##.##").format(hasil)));
                break;
        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        hasil();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }
}
